let average = (...rest)=>{
    let sum = 0;
    let result = 0;
    for(let num of rest){
        sum += num;
    }
    result = sum / rest.length;
    return result;
}
let khoi1 = ()=>{
    let toan = document.querySelector('#inpToan').value*1; 
    let ly = document.querySelector('#inpLy').value*1; 
    let hoa = document.querySelector('#inpHoa').value*1; 

    document.querySelector('#tbKhoi1').innerHTML = average(toan,ly,hoa);
}
let khoi2 = ()=>{
    let van = document.querySelector('#inpVan').value*1; 
    let su = document.querySelector('#inpSu').value*1; 
    let dia = document.querySelector('#inpDia').value*1; 
    let anh = document.querySelector('#inpEnglish').value*1; 
    
    document.querySelector('#tbKhoi2').innerHTML = average(van,su,dia,anh);
}
document.querySelector('#btnKhoi1').addEventListener('click',khoi1)
document.querySelector('#btnKhoi2').addEventListener('click',khoi2)
