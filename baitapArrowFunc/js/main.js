const colors = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

const renderButton = () =>{
    let content = '';
    for (let value of colors){
        content +=  `<button class="btn color-button ${value}" style="background-color:${value};" onclick="changeColor('${value}')"></button>`;
    }
    document.getElementById('colorContainer').innerHTML = content;
}
const changeColor = (color)=>{
    document.querySelector('.house').classList.add(`${color}`);
}
renderButton()


