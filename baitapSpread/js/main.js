let hover = ()=>{
    let text = document.querySelector('.heading').innerText;
    let chars = [...text];
    let result = '';
    for(let char of chars){
        let content = `<span>${char}</span>`;
        result += content;
    }
    return result;
}
document.querySelector('.heading').innerHTML= hover();